import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, LoadingController } from '@ionic/angular';
import { Environment, 
  GoogleMaps, 
  GoogleMap, 
  GoogleMapOptions, 
  GoogleMapsEvent, 
  MyLocation, 
  GoogleMapsAnimation,
  LatLng,
  MarkerOptions,
  Marker} from '@ionic-native/google-maps';

  declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  [x: string]: any;
  @ViewChild('map', {static:true}) mapElement: any;
  private loading: any;
  private map: GoogleMap;
  
  constructor(
    private platform: Platform,
    private loadingCtrl: LoadingController
    ) { } 

  ngOnInit (){
    this.mapElement = this.mapElement.nativeElement;

    this.mapElement.style.width = this.platform.width() + 'px';
    this.mapElement.style.height = this.platform.height() + 'px';

    this.loadMap();
  }

  async loadMap(){
    this.loading = await this.loadingCtrl.create({ message: "Aguarde..."});
    await this.loading.present();

    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyA4AXFaADvHIjy5LbPNASb6TPh9u6wTnto',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyA4AXFaADvHIjy5LbPNASb6TPh9u6wTnto'
    });

    const mapOptions: GoogleMapOptions = {
      controls: {
        zoom: false
      }
    };

    this.map = GoogleMaps.create(this.mapElement, mapOptions);
    
    try {
      await this.map.one(GoogleMapsEvent.MAP_READY);

      this.addOriginMarker();  
    } catch(error) {
      console.error(error);
    }
  }

  async addOriginMarker(){
    try {
      const myLocation: MyLocation = await this.map.getMyLocation();
      
      await this.map.moveCamera({
        target: myLocation.latLng,
        zoom: 15
      });

      this.map.addMarkerSync({
        title: 'Seu Local',
        icon: '#FF0000',
        animation: GoogleMapsAnimation.DROP,
        position: myLocation.latLng
      });
    } catch(error) {
      console.error(error);
    }finally {
      this.loading.dismiss();
    }
  }
}
